
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from models import Schedule
from forms import SubmitSchedule



def schedules(request):
    """
    Listing all the available schedules.
    """
    context  = {'form': SubmitSchedule, 'schedules': Schedule.objects.all()}

    return render(request, 'schedules.tpl', context)


def submit_schedule(request):
    """
    Submitting a new schedule.
    """
    response = {'success': False}
    if request.method == "POST":
        form = SubmitSchedule(request.POST)

        if form.is_valid():
            form.save()
            response.update({
                'success': True
            })
        else:
            response.update({
                'errors': form.errors
            })

    return JsonResponse(response)
