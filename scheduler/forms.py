
import datetime
from django import forms

from models import Schedule


def clean_time(time_str, field_name):
    """
    Method to clean time.
    """
    try:
        date_obj = datetime.datetime.strptime(time_str, "%H:%M")
    except Exception:
        raise forms.ValidationError("Time format is invalid, try HH24:MM format")
    else:
        return time_str


class SubmitSchedule(forms.ModelForm):

    class Meta:
        model = Schedule
        exclude = ['scheduled_at']

    def __init__(self, *args, **kwargs):
        super(SubmitSchedule, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs\
            .update({
                'placeholder': 'mm/dd/yyyy',
            })

        self.fields['end_date'].widget.attrs\
            .update({
                'placeholder': 'mm/dd/yyyy',
            })

        self.fields['start_time'].widget.attrs\
            .update({
                'placeholder': 'hh:mm (24 hrs format)',
            })

        self.fields['end_time'].widget.attrs\
            .update({
                'placeholder': 'hh:mm (24 hrs format)',
            })

        self.fields['scheduled_by'].widget.attrs\
            .update({
                'placeholder': 'Name of the person',
            })

    def clean_start_time(self):
        start_time = self.cleaned_data.get('start_time')
        return clean_time(start_time, 'start_time')

    def clean_end_time(self):
        end_time = self.cleaned_data.get('end_time')
        return clean_time(end_time, 'end_time')

