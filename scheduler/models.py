from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class ScheduleManager(models.Manager):
    def get_queryset(self):
        return super(ScheduleManager, self).get_queryset().order_by('-scheduled_at')

class Schedule(models.Model):

    """
    Schedule model.
    """

    title = models.CharField(max_length=250)
    description = models.TextField()
    scheduled_by = models.CharField(max_length=50)
    scheduled_at = models.DateTimeField(default=timezone.now())
    start_date = models.DateField()
    start_time = models.CharField(max_length=10)
    end_date = models.DateField()
    end_time = models.CharField(max_length=10)

    objects = ScheduleManager()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        self.scheduled_at = timezone.now()

        return super(Schedule, self).save(*args, **kwargs)
